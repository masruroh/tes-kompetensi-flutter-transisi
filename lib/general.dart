import 'package:flutter/material.dart';

String urlBase = 'https://reqres.in/', urlAPI = 'https://reqres.in/api/';
Color tema = Colors.blue;
Color abu = Color(0XFFD3D8E1); // abu"

double jarak = 10, rad = 10.0, elev = 5.0;
double widthTom = 100;
List<double> sizeF = [15, 17, 20, 25];
var header = <String, String>{
  'Content-Type': 'application/json; charset=UTF-8'
};
style(double size, {Color color = Colors.black, bool b = false}) {
  return TextStyle(
      fontSize: size,
      color: color,
      fontWeight: (b) ? FontWeight.bold : FontWeight.normal);
}

tombol(String title, Function onpressed,
    {bool putih = true, double width, Color color}) {
  return Container(
      child: MaterialButton(
          child: Text(
            title,
            style: style(sizeF[0],
                color: (putih) ? Colors.white : Colors.black, b: true),
          ),
          color: (color == null) ? tema : color,
          minWidth: (width == null) ? widthTom : width,
          onPressed: onpressed));
}

alert(BuildContext context, {String content, Function ok}) {
  return showDialog(
    context: context,
    builder: (context) {
      return StatefulBuilder(
        builder: (context, setState) {
          return AlertDialog(
            title: Text('Notification', style: style(20, b: true)),
            content: Form(
              child: SingleChildScrollView(
                child: Column(children: [
                  Text(content, style: style(15)),
                  tombol(
                      'OK',
                      (ok == null)
                          ? () {
                              Navigator.of(context).pop();
                            }
                          : ok)
                ]),
              ),
            ),
          );
        },
      );
    },
  );
}

// UNTUK LOADING
var load = Center(
  child: Container(
      width: 150, child: Image.asset('assets/loading.gif', fit: BoxFit.cover)),
);
loading() {
  return SafeArea(child: Scaffold(body: load));
}
