import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:test_transisi/employee/list.dart';
import 'package:http/http.dart' as http;

import 'general.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool _obscureText = true;
  String usernameString = '', passwordString = '';
  TextEditingController usernameTEC, passwordTEC;
  bool get = true;
  login() async {
    setState(() => get = false);
    String url = urlAPI + 'login';
    var body = jsonEncode(
        <String, String>{'email': usernameString, 'password': passwordString});

    await http.post(Uri.parse(url), headers: header, body: body).then(
      (http.Response response) {
        setState(() => get = true);
        var temp = jsonDecode(response.body);
        if (temp['token'] != null) {
          // kalau berhasil
          Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (_) {
            return EmployeeList();
          }));
        } else {
          alert(context, content: temp['error']);
        }
      },
    );
  }

  _decide() {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.lightBlue,
        body: Stack(
          children: [
            Align(
              alignment: Alignment(0, 0),
              child: SingleChildScrollView(
                child: Container(
                  margin: EdgeInsets.all(jarak * 2),
                  child: Material(
                    elevation: elev,
                    borderRadius: BorderRadius.circular(rad),
                    child: Container(
                      padding: EdgeInsets.all(jarak * 2),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          // // Container(
                          // //     child: Image.asset('assets/logo.png',
                          // //         height: 100, fit: BoxFit.cover)),
                          // SizedBox(height: jarak * 2),
                          Container(
                            // padding: EdgeInsets.only(left: jarak, right: jarak),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(rad * 2),
                              color: Colors.lightBlue,
                            ),
                            child: TextField(
                              controller: usernameTEC,
                              decoration: InputDecoration(
                                prefixIcon: Icon(Icons.person,
                                    color: Colors.black.withOpacity(0.5)),
                                hintText: "Email",
                                border: InputBorder.none,
                              ),
                              keyboardType: TextInputType.emailAddress,
                              onChanged: (value) {
                                setState(() => usernameString = value);
                              },
                            ),
                          ),
                          SizedBox(height: jarak),
                          Container(
                            // padding: EdgeInsets.only(left: jarak, right: jarak),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(rad * 2),
                              color: Colors.lightBlue,
                            ),
                            child: TextField(
                              controller: passwordTEC,
                              obscureText: _obscureText,
                              decoration: InputDecoration(
                                prefixIcon: Icon(Icons.lock,
                                    color: Colors.black.withOpacity(0.5)),
                                hintText: "Password",
                                border: InputBorder.none,
                                suffixIcon: GestureDetector(
                                    child: Icon(
                                      (_obscureText)
                                          ? Icons.visibility
                                          : Icons.visibility_off,
                                      // color: Colors.black.withOpacity(0.5),
                                    ),
                                    onTap: () {
                                      setState(
                                          () => _obscureText = !_obscureText);
                                    }),
                              ),
                              onChanged: (value) {
                                setState(() => passwordString = value);
                              },
                            ),
                          ),
                          SizedBox(height: jarak * 2),
                          tombol('LOGIN', () {
                            login();
                          }),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return (get) ? _decide() : loading();
  }
}
