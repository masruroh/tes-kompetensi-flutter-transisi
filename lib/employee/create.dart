import 'dart:convert';

import 'package:flutter/material.dart';

import '../general.dart';
import 'package:http/http.dart' as http;

class EmployeeCreate extends StatefulWidget {
  @override
  _EmployeeCreateState createState() => _EmployeeCreateState();
}

class _EmployeeCreateState extends State<EmployeeCreate> {
  String emailString = '', passwordString = '', telefoneString = '';
  TextEditingController nameTEC,
      apelidoTEC,
      trabalhoTEC,
      telefoneTEC,
      emailTEC,
      siteTEC;
  bool get = true;
  addData() async {
    setState(() => get = false);
    String url = urlAPI + 'register';
    var body = jsonEncode(
        <String, String>{'email': emailString, 'password': passwordString});

    await http.post(Uri.parse(url), headers: header, body: body).then(
      (http.Response response) {
        var temp = jsonDecode(response.body);
        print(temp);
        setState(() => get = true);
        if (temp['token'] != null) {
          // kalau berhasil
          alert(context, content: 'Successfully Registered', ok: () {
            Navigator.of(context).pop();
            Navigator.of(context).pop();
          });
        } else {
          alert(context, content: temp['error']);
        }
      },
    );
  }

  _decide() {
    return SafeArea(
        child: Scaffold(
      appBar: AppBar(
        title: Text('Criar novo contato'),
        actions: [
          MaterialButton(
              onPressed: () => addData(),
              child: Text('SALVAR'),
              textColor: Colors.white),
        ],
        leading: IconButton(
            icon: Icon(Icons.close),
            onPressed: () => Navigator.of(context).pop()),
      ),
      body: SingleChildScrollView(
        child: Container(
          width: double.infinity,
          padding: EdgeInsets.all(jarak),
          child: Column(
            children: [
              SizedBox(height: jarak),
              Container(
                width: 100,
                height: 100,
                decoration: BoxDecoration(shape: BoxShape.circle, color: tema),
                child: Icon(Icons.camera_alt, color: Colors.white),
              ),
              SizedBox(height: jarak),
              TextFormField(
                controller: nameTEC,
                decoration: InputDecoration(
                    hintText: 'Name', prefixIcon: Icon(Icons.person)),
              ),
              SizedBox(height: jarak),
              TextFormField(
                controller: apelidoTEC,
                decoration: InputDecoration(
                    hintText: 'Apelido', prefixIcon: Icon(Icons.person)),
              ),
              SizedBox(height: jarak),
              TextFormField(
                // controller: apelidoTEC,
                onChanged: (value) => setState(() => passwordString = value),
                decoration: InputDecoration(
                    hintText: 'Password', prefixIcon: Icon(Icons.lock)),
                obscureText: true,
              ),
              SizedBox(height: jarak),
              TextFormField(
                controller: trabalhoTEC,
                decoration: InputDecoration(
                    hintText: 'Trabalho', prefixIcon: Icon(Icons.badge)),
              ),
              SizedBox(height: jarak),
              TextFormField(
                controller: telefoneTEC,
                onChanged: (value) => setState(() => telefoneString = value),
                keyboardType: TextInputType.phone,
                decoration: InputDecoration(
                    hintText: 'Telefone',
                    prefixIcon: Icon(Icons.local_phone),
                    counterText: telefoneString.length.toString() + '/16'),
              ),
              SizedBox(height: jarak),
              TextFormField(
                controller: emailTEC,
                onChanged: (value) => setState(() => emailString = value),
                keyboardType: TextInputType.emailAddress,
                decoration: InputDecoration(
                    hintText: 'E-mail', prefixIcon: Icon(Icons.email)),
              ),
              SizedBox(height: jarak),
              TextFormField(
                controller: siteTEC,
                decoration: InputDecoration(
                    hintText: 'Site da Web', prefixIcon: Icon(Icons.web)),
              ),
            ],
          ),
        ),
      ),
    ));
  }

  @override
  Widget build(BuildContext context) {
    return (get) ? _decide() : loading();
  }
}
