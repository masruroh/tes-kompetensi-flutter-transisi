import 'package:flutter/material.dart';
import '../class/employee.dart';
import '../general.dart';

class EmployeeDetail extends StatefulWidget {
  const EmployeeDetail({this.data});
  final Employee data;
  @override
  _EmployeeDetailState createState() => _EmployeeDetailState();
}

class _EmployeeDetailState extends State<EmployeeDetail> {
  contData(Icon icon, String title, String subtitle, {Icon trailing}) {
    return Container(
      margin: EdgeInsets.only(bottom: jarak),
      child: Material(
        elevation: 5,
        borderRadius: BorderRadius.circular(10),
        child: Container(
            padding: EdgeInsets.all(jarak),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(children: [
                  icon,
                  SizedBox(width: jarak),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(title, style: style(18)),
                      Text(subtitle, style: style(17, color: Colors.grey))
                    ],
                  )
                ]),
                SizedBox(width: jarak),
                (trailing == null) ? Container() : trailing
              ],
            )),
      ),
    );
    // return ListTile(
    //     leading: icon,
    //     title: Text(title),
    //     subtitle: Text(subtitle),
    //     trailing: (trailing == null) ? Container() : trailing);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                  color: tema,
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          IconButton(
                              icon: Icon(Icons.arrow_back, color: Colors.white),
                              onPressed: () {
                                Navigator.of(context).pop();
                              }),
                          Row(
                            children: [
                              IconButton(
                                  icon: (widget.data.active)
                                      ? Icon(Icons.star, color: Colors.white)
                                      : Icon(Icons.star_border,
                                          color: Colors.white),
                                  onPressed: () {
                                    setState(() {
                                      widget.data.active = !widget.data.active;
                                    });
                                  }),
                              IconButton(
                                  icon: Icon(Icons.edit, color: Colors.white),
                                  onPressed: () {}),
                            ],
                          ),
                        ],
                      ),
                      SizedBox(height: jarak * 2),
                      Icon(Icons.person, size: 125, color: Colors.white),
                      SizedBox(height: jarak),
                      Text(widget.data.firstname + ' ' + widget.data.lastname,
                          style: style(25, color: Colors.white)),
                      SizedBox(height: jarak * 2),
                    ],
                  )),
              Container(
                  padding: EdgeInsets.all(jarak),
                  child: Column(
                    children: [
                      contData(Icon(Icons.local_phone, color: tema),
                          widget.data.phone, 'Telefone',
                          trailing: Icon(Icons.chat, color: Colors.grey)),
                      contData(Icon(Icons.email, color: tema),
                          widget.data.email, 'E-mail'),
                      contData(Icon(Icons.share, color: tema), widget.data.web,
                          'Compartilhar'),
                    ],
                  )),
            ],
          ),
        ),
      ),
    );
  }
}
