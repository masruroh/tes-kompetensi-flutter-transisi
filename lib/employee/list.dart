import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:test_transisi/employee/create.dart';
import 'package:http/http.dart' as http;
import '../class/employee.dart';

import '../general.dart';
import 'detail.dart';

class EmployeeList extends StatefulWidget {
  @override
  _EmployeeListState createState() => _EmployeeListState();
}

class _EmployeeListState extends State<EmployeeList> {
  bool get = false;
  List<Employee> listData;
  getData() async {
    print('proses geData');
    setState(() => get = false);

    await http.get(Uri.parse(urlAPI + 'users?page=2'), headers: header).then(
      (http.Response response) {
        setState(() => get = true);
        // FUNGSI
        var temp = jsonDecode(response.body);
        // print(temp);
        setListData(temp['data']);
      },
    );
  }

  setListData(List data) {
    setState(() => listData = []);
    for (var i = 0; i < data.length; i++) {
      listData.add(
        Employee(
            urut: i,
            id: data[i]['id'].toString(),
            email: data[i]['email'],
            firstname: data[i]['first_name'],
            lastname: data[i]['last_name'],
            job: 'Programmer',
            phone: '(999) 99999-9999',
            web: 'linkedin.com/in/izzatul-masruroh',
            active: (i % 2 == 0) ? true : false),
      );
    }
  }

  contData(Employee data) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => EmployeeDetail(data: data)))
            .then(_goBack);
      },
      child: ListTile(
          leading: CircleAvatar(
              child: Text(data.firstname[0]), backgroundColor: tema),
          title: Text(data.firstname + ' ' + data.lastname),
          subtitle: Text(data.phone),
          trailing: GestureDetector(
              onTap: () => setState(() => data.active = !data.active),
              child: (data.active)
                  ? Icon(Icons.star, color: tema)
                  : Icon(Icons.star_border, color: abu))),
    );
  }

  _decide() {
    return SafeArea(
        child: Scaffold(
      appBar: AppBar(title: Text('Contatos'), actions: [
        Container(
            padding: EdgeInsets.only(right: jarak), child: Icon(Icons.search))
      ]),
      floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.push(context,
                    MaterialPageRoute(builder: (context) => EmployeeCreate()))
                .then(_goBack);
          },
          child: Icon(Icons.add)),
      body: ListView.builder(
        itemCount: listData.length,
        itemBuilder: (context, i) {
          return contData(listData[i]);
        },
      ),
    ));
  }

  FutureOr _goBack(dynamic value) async {
    getData();
  }

  @override
  void initState() {
    super.initState();
    setState(() => listData = []);
    getData();
  }

  @override
  Widget build(BuildContext context) {
    return (get) ? _decide() : loading();
  }
}
