class Employee {
  Employee(
      {this.urut,
      this.id,
      this.email,
      this.firstname,
      this.lastname,
      this.phone,
      this.job,
      this.web,
      this.active});
  int urut;
  String id, email, firstname, lastname, phone, job, web;
  bool active;
}
