# Tes Kompetensi Flutter Transisi

Tes Kompetensi Flutter Transisi adalah aplikasi sederhana menggunakan flutter

## Getting Started

File apk terdapat pada folder apk

Aplikasi ini menggunakan REST API pada link 'https://reqres.in/'

Login dan Register menggunakan alamat email eve.holt@reqres.in sesuai dengan dokumentasi REST API tersebut. 

### Tampilan login page
![get data](assets/blob/login.jpeg )

### Tampilan home page
Icon bintang di setiap data bisa di klik. nilai default saya buat index genap aktif dan index ganjil tidak aktif
![get data](assets/blob/list.jpeg )

### Tampilan create page
![get data](assets/blob/create.jpeg )

### Tampilan detail page
![get data](assets/blob/detail.jpeg )
